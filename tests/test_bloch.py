import numpy as np

import mmf_setup

mmf_setup.set_path()

from phys_555_2022 import bloch

import pytest


@pytest.fixture
def rng(seed=2):
    return np.random.default_rng(seed=seed)


class TestState:
    def test_conversions(self, rng):
        theta = rng.random() * np.pi
        phi = rng.random() * 2 * np.pi

        ket = np.array([np.cos(theta / 2), np.sin(theta / 2) * np.exp(1j * phi)])
        a = np.array(
            [np.sin(theta) * np.cos(phi), np.sin(theta) * np.sin(phi), np.cos(theta)]
        )
        rho = np.outer(ket, ket.conj())

        s = bloch.State(ket=ket)
        assert np.allclose(a, s.get_vector(density_matrix=rho))
        assert np.allclose(a, s.get_vector(ket=ket))
        assert np.allclose(ket, s.get_ket(vector=a))
        assert np.allclose(ket, s.get_ket(density_matrix=rho))
        assert np.allclose(rho, s.get_density_matrix(vector=a))
        assert np.allclose(rho, s.get_density_matrix(ket=ket))
        assert np.allclose(s.vector, a)
        assert np.allclose(s.density_matrix, rho)
        assert np.allclose(s.ket, ket)
