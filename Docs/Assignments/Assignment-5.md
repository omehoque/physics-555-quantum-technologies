# No Cloning

Prove the no-cloning theorem.

Figure 1.16 on page 30 of {cite:p}`Nielsen:2010` shows ꜰᴀɴᴏᴜᴛ.  This seems to violate
the no-cloning theorem by duplicating the state $a$.  Explain.
