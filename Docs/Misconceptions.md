(sec:misconceptions)=
# Quantum Misconceptions

Quantum mechanics has a reputation of being difficult and the source of many mysterious
paradoxes.  While there are truly remarkable and perplexing consequences of the quantum
formalism, many of the seemingly impossible claims are just that: impossible
consequences of misapplying the fundamental principals.

Here we list some common misconceptions.  Can you see where the presentations have mistakes?

## Quantum Eraser and Retrocausality

The delayed-choice quantum eraser seems to violate causality by allowing a measurement
in the future to change the result of a previous measurement (retrocausality).  There is
no real paradox here: quantum mechanics is completely consistent with causality.  Can
you find the mistakes made in the following presentations that lead the presenters to
(incorrectly) conclude that quantum mechanics allows retrocausal effects?

* [How the Quantum Eraser Rewrites the Past | Space Time | PBS Digital
  Studios](https://youtu.be/8ORLN_KwAgs) 
* [Down The Rabbit Hole Of The Delayed Choice Quantum Eraser | Answers With
  Joe](https://youtu.be/spKlpexL_Hg)
* [The super bizarre quantum eraser experiment | Fermilab](https://youtu.be/l8gQ5GNk16s)

### Explanation

Sabine Hossenfelder does a reasonable job of "debunking" these videos in [The Delayed
Choice Quantum Eraser, Debunked], as does Sean Carroll in his blog post [The Notorious
Delayed-Choice Quantum Eraser].  Before viewing these, try to figure out where the
previous expositions get it wrong.

[The Delayed Choice Quantum Eraser, Debunked]: <https://youtu.be/RQv5CVELG3U>
[The Notorious Delayed-Choice Quantum Eraser]: <https://www.preposterousuniverse.com/blog/2019/09/21/the-notorious-delayed-choice-quantum-eraser/>


## Quantum Computers are Faster than Classical Computers

:::{margin}
Incomplete.  This section needs some references, both to examples of the misconception,
and to careful characterize the speed of quantum computers.
:::
Another misconception is that quantum computers will be faster classical computers.
This is unlikely to be true.  In general, quantum computers will be **slower** than
their classical counterparts in order to preserve coherence.

### Explanation

Quantum advantage is associated with certain problems where quantum quantum algorithms
have an algorithmic advantage over classical computers, and hence will be able to solve
certain larger problems than classical computers (if they can be made to be large
enough).

For example, using Grover's algorithm, quantum computers can perform an unstructured
search over $N$ in time $O(\sqrt{N})$, which is provably better than classical
algorithms which must at least read each entry, thus taking at least $O(N)$ time.  Note
that this proof addresses the **asymptotic time complexity** of the algorithms, but
specifies nothing about the prefactor.  Almost certainty, the (machine-dependent)
prefactor for Grover's algorithm will be significantly larger.  Thus, once $N$ is large
enough, the quantum algorithm is guaranteed to win, but we must make a large enough
quantum computer that can maintain sufficient coherence to realize this advantage.



