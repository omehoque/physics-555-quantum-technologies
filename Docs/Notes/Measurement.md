---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.0
kernelspec:
  display_name: Python 3 (phys-555)
  language: python
  name: phys-555
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:Measurement)=
# Measurement

## Generalized Measurement

{cite:p}`Nielsen:2010` define measurements in terms of a set of [Kraus operators][]
$\{\mat{M}_{m}\}$ such that

\begin{gather*}
  \sum_{m} \mat{M}_m^\dagger \mat{M}_m = \sum_{m}\mat{E}_{m} = \mat{1}\\
  \mat{E}_m = \mat{M}_m^\dagger \mat{M}_m.
\end{gather*}

:::{note}
**The measurement** -- a single measurement -- consists of the **entire set**
$\{\mat{M}_{m}\}$ of [Kraus operators][], not the individual operators.  Each operator
represents a potential result or outcome of the measurement, labeled by the index $m$,
but the measurement must be considered in terms of the entire set, which is subject to
the completeness relationship above.
:::

In terms of a state $\ket{\psi}$, the act of such a measurement is **a result** $m$
obtained with probability $p_m$ which transforms the state as $\ket{\psi} \rightarrow
\mat{M}_{m}\ket{\psi}$ (suitably normalized):

\begin{gather*}
  p_m = \braket{\psi|\mat{E}_{m}|\psi} 
      = \braket{\psi|\mat{M}_{m}^\dagger \mat{M}_{m}|\psi}, 
  \qquad
  \ket{\psi} \rightarrow \frac{\mat{M}_{m}\ket{\psi}}{\sqrt{p_m}}.
\end{gather*}

## POVM

Note that, if we do not care about the evolution of the state vector, then it suffices
to consider only the set of **positive operators** $\mat{E}_m$, which is called a
**positive operator-valued measurement**, or [POVM][].  This is simply a complete set
$\{\mat{E}_{m}\}$ of positive operators such that

\begin{gather*}
  \sum_m{\mat{E}_m} = \mat{1}
\end{gather*}

where the probability of obtaining outcome $m$ when measuring a state $\ket{\psi}$ is
$p_m = \braket{\psi|\mat{E}_m|\psi}$ as stated above.  The evolution of the state is
ill-specified, but can be unitarily obtained from $\sqrt{\mat{E}_m}\ket{\psi}$ (suitably
normalized).  I.e. the [POVM][] represents a family of generalized measurements of the form:

\begin{gather*}
  \mat{M}_m = \mat{U}\sqrt{\mat{E}_{m}}.
\end{gather*}

## Projective measurements

If the positive operators $\mat{E}_m = \mat{P}_m = \mat{P}_m^2$ are
{ref}`sec:Projections` (i.e. idempotent), then the measurement is said to be a
projective measurement (related to projection-valued measures or [PVM][]s).  The key
difference is that we can now take $\mat{M}_m = \mat{P}_m$, and so the measurement is
**repeatable**: if we measure a system and obtain a value $m$, then quickly measuring the system
again will yield the same value $m$.

### von Neumann observables

Projective measurements are the measurements most familiar to physicists where they are
usually specified in terms of a hermitian operator $\mat{O}$ call an **observable** or a
**von Neumann observable**.  Here one defines the outcomes of the measurement $m$ to be
the eigenvalues, and constructs the projectors from the eigenvectors:

\begin{gather*}
  \mat{O}\ket{m} = \ket{m}m, \qquad
  \mat{P}_{m} = \frac{\ket{m}\!\bra{m}}{\braket{m|m}}.
\end{gather*}

Since hermitian operators have a complete set of orthonormal eigenvectors, the
completeness requirement is satisfied.  Note: if there are degenerate eigenvalues, then
the projector should include all of the eigenvectors

\begin{gather*}
  \mat{O}\ket{m_i} = \ket{m_i}m, \qquad
  \mat{P}_{m} = \sum_{i}\ket{m_i}\!\bra{m_i}.
\end{gather*}

From a projective measurement, the observable is simply

\begin{gather*}
  \mat{O} = \sum_{m} m \mat{P}_{m}.
\end{gather*}

Thus, when discussing projective measurements, one often refers to simply **measuring
the operator $\mat{O}$** or **measuring $\mat{O}$**.  Thus, we refer to measuring
$\mat{Z}$ when measuring $\{\mat{E}_0 = \ket{0}\!\bra{0}, \mat{E}_1 =
\ket{1}\!\bra{1}\}$ in the standard basis.

## Weak Measurements

:::{margin}
This is true if the projector has rank 1.  If the projector spans a large subspace,
measurements within that subspace can yield more information.
:::
Projective measurements and von Neumann observables cause the state to collapse so that
subsequent measurements give the same result, yielding no more information about the
state.  These are sometimes called **strong measurements**.  The generalized measurement
framework allows also for the notion of a [weak measurement][], which does not fully
collapse the state.  We discuss these in Section {ref}`sec:WeakMeasurements`.

[likelihood]: <https://en.wikipedia.org/wiki/Likelihood_function>
[Bayes' theorem]: <https://en.wikipedia.org/wiki/Bayes%27_theorem>
[Bloch sphere]: <https://en.wikipedia.org/wiki/Bloch_sphere> 
[qubit]: <https://en.wikipedia.org/wiki/Qubit>
[spherical coordinates]: <https://en.wikipedia.org/wiki/Spherical_coordinate_system>
[PVM]: <https://en.wikipedia.org/wiki/Projection-valued_measure>
[weak measurement]: <https://en.wikipedia.org/wiki/Weak_measurement>
[Kraus operators]: <https://en.wikipedia.org/wiki/Quantum_operation#Kraus_operators>
[section 4.5.2]: <https://ntserver1.wsulibs.wsu.edu:2171/lib/wsu/reader.action?docID=647366&ppg=225>
