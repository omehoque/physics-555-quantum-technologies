---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.0
kernelspec:
  display_name: Python 3 (phys-555)
  language: python
  name: phys-555
---

# Bloch Sphere

```{code-cell} ipython3
%matplotlib inline
from ipywidgets import interact
import mmf_setup;mmf_setup.nbinit(quiet=True)
from importlib import reload
from phys_555_2022 import bloch;reload(bloch)

s = bloch.BState(vector=[0, 1, 1], Bvector=[0,0,1], normalize=True)
@interact(Bx=(-1.0, 1.0, 0.1), Bz=(-1.0, 1.0, 0.1))
def go(Bx=0, Bz=0.0):
    s.Bvector = [Bx, 0, Bz]
    #display(s.ket)
    #display(s.vector)
    #display(s.density_matrix)
s.out
```

```{code-cell} ipython3
anim = s.animate()
anim.start()
```

```{code-cell} ipython3
@interact(Bz=(-1.0, 1.0, 0.1))
def go(Bz=0.0):
    s.Bvector = [0, 0, Bz]    
```

```{code-cell} ipython3

```

```{code-cell} ipython3
from mmfutils.contexts import NoInterrupt
from ipywidgets import interact
from ipywidgets.widgets import FloatSlider, VBox

_kw = dict(min=-2.0, max=2.0, step=0.1)
B = [FloatSlider(0, description="Bx", **_kw), 
     FloatSlider(0, description="By", **_kw), 
     FloatSlider(1.0, description="Bz", **_kw)]
VBox(B)
```

```{code-cell} ipython3
NoInterrupt.unregister()
with NoInterrupt() as interrupted:
    for n in range(1000):
        if interrupted:
            break
        s.rotate(0.1, [0,1,0])
```

```{code-cell} ipython3
s.
```

```{code-cell} ipython3

```
