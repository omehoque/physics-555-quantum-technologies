# Live Demos

These notebooks feature interactive demonstrations used in lectures and presentations.  They are not really suitable for inclusion in the static documentation.