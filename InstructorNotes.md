Instructor Notes
================

:::{important}
Remember that these notes are shared publicly, so please be careful not to include any
private information unless that information is linked beyond a proper authorization
screen (for example, links to the course Canvas site or CoCalc sites are fine because
students must authenticate.)
:::

## Content

### Potentially Interesting Readings

* ['t Hooft: "The Free-Will Postulate in Quantum Mechanics"](https://arxiv.org/abs/quant-ph/0701097)

## Course Setup

To setup the course I did the following:

### Create Repositories on GitLab, CoCalc, etc.

The first step is to create the various online repositories we will use for the course.

1. Create a project on [GitLab][], i.e. [GitLab course repo][].
   * Use an appropriate group ([wus-courses](https://gitlab.com/wsu-courses/) for
     example).
   * Make it public so we can create a documentation link (see below).
   * Add a brief but relevant description.
2. (optional) Create a mirror on [GitHub][], i.e. [GitHub course mirror][].
3. Create a documentation project on [ReadTheDocs][], i.e. [course documentation admin page][].
   * You can tell [ReadTheDocs][] to scan your [GitLab][] and [GitHub][] public repos,
     hence, if you made the main repo public above, then you should see it.
   * When you create the project, make the name short -- this will ensure that the
     auto-generated slug and URL is not too long, which can cause problems.  You can
     change this back to the full course name after you create the slug.
4. (optional) Create a [CoCalc][] course file, i.e. [CoCalc course file][].
   * Open the course file and create the Shared Project and open it, i.e. [CoCalc shared project][].

### Create the Project with Cookiecutter

The next step is to clone these repositories to your computer, then to use our [cookiecutter
templates][] to get the code and documentation setup.

1. Clone your main repo, and copy the closest course file from the [cookiecutter
   templates][].  (I use mercurial through the [hg-git][] bridge below, but you can use
   `git` directly if you prefer.) 

   ```bash
   hg clone git@gitlab.com:wsu-courses/physics-555-quantum-technologies.git Course
   cd Course
   wget -O physics-555.yaml https://hg.iscimath.org/forbes-group/cookiecutter-templates/-/raw/topic/default/initial/cookiecutter-course/physics-555.yaml?inline=false 
   mv physics-555.yaml .physics-555.cookiecutter.yaml
   ```
   
   Edit this `yaml` file to reflect the course using the URL's etc. you found above, as
   well as the course information as listed in the [WSU Schedule of Classes][].
6. Create a branch called `cookiecutter-base`, then add and commit the updated file.

   ```bash
   hg add .physics-555.cookiecutter.yaml
   hg branch cookiecutter-base
   hg com -m "BASE: Added initial yaml file."
   ```
   
   The idea is to use the `cookiecutter-base` branch to incorporate any improvements
   made to the skeleton cookiecutter files.  Once these changes are reviewed and
   updated, we can pull the changes into the `main` branch and our version control
   system should be able to merge things.
   
7. Generate the course skeleton using cookiecutter.  Currently, there is no way to
   specify the output directory, so you need to do something to make the current
   directory look like the project slug, `phys-555-quantum-technologies` in this case.
   Here we do this twice: first to get the name, then we make a symlink to the current
   directory.

   ```bash
   # Need version 2.0.2 which is not yet on PyPI.
   pip install --upgrade --user git+https://github.com/cookiecutter/cookiecutter.git@2.0.2#egg=cookiecutter=2.0.2
   cookiecutter hg+https://hg.iscimath.org/forbes-group/cookiecutter-templates \
                --directory cookiecutter-course                                \
                --config-file .physics-555.cookiecutter.yaml                   \
                --output-dir .                                                 \
                --overwrite-if-exists                                          \
                --no-input
   ls
   # See that everything was put into phys-555-quantum-technologies
   ```
   
   Now we remove it, then symlink the current directory to `.` and repeat.  Finally, we
   move the replay file here so we have a record. (There might be a better way using
   [post-install hooks](https://github.com/cookiecutter/cookiecutter/issues/1475).)
   
   ```bash
   rm -rf phys-555-quantum-technologies
   ln -s . phys-555-quantum-technologies
   cookiecutter hg+https://hg.iscimath.org/forbes-group/cookiecutter-templates \
                --directory cookiecutter-course                                \
                --config-file .physics-555.cookiecutter.yaml                   \
                --output-dir .                                                 \
                --overwrite-if-exists                                          \
                --no-input
   mv ~/.cookiecutter_replay/cookiecutter-course.json .physics-555.cookiecutter-replay.json
   ```
   
8. Commit this to the `cookiecutter-base` branch:

   ```bash
   hg add
   hg com -m "BASE: Cookiecutter skeleton."
   ```
   
9. Merge these changes back into the main branch:

   ```bash
   hg up main
   hg merge cookiecutter-base
   ```

10. To update in the future, if the cookiecutter template changes:

   ```bash
   # Make sure your work in main is committed.
   hg com ...

   # Switch to cookiecutter-base branch
   hg up cookiecutter-base
   
   # Update.... this currently fails because of the following issues.  You may need to
   # remove these manually.
   # https://github.com/cookiecutter/cookiecutter/issues/1650
   cookiecutter hg+https://hg.iscimath.org/forbes-group/cookiecutter-templates \
     --directory cookiecutter-course --config-file .physics-555.cookiecutter.yaml -o . -f --no-input
   hg add
   hg com -m "BASE: Updated cookiecutter skeleton"
   hg up main
   hg merge cookiecutter-base
   
   # Resolve any issues...
   
   hg com -m "Merge updated skeleton to main branch"
   ```
   
   :::{note}
   Currently this fails due to [issue
   1176](https://github.com/cookiecutter/cookiecutter/issues/1176).  You will need to
   manually remove any directories i.e.:
   
   ```bash
   rm -rf Docs/_templates Docs/_static
   cookiecutter hg+https://hg.iscimath.org/forbes-group/cookiecutter-templates \
     --directory cookiecutter-course --config-file .physics-555.cookiecutter.yaml -o . -f --no-input
   ```
   :::

### Edit the Documentation

At this point, you should be able to build and inspect the base documentation.  To do
this, and edit the documentation, you can run

```bash
make init
make doc-server
```

which will serve the documentation on http://localhost:8000.  If you leave this running,
you should be able to edit the various [MyST][] markdown files in `Docs/`, and
these updates should be incorporated into the documentation.  This system uses the
following components, which you should review to understand how the documentation works:

:::{admonition} Documentation References and Tools
:class: dropdown

* [Jupyter Book][]: The documentation is generally governed by the tools and philosophy
  behind the [Jupyter Book][] project: an open source project for building beautiful,
  publication-quality books and documents from computational material.
* [Sphinx][]: This is the underlying documentation system which builds and links all of
  the documentation into the final HTML or PDF files.  It is configured in the file
  `Docs/conf.py` and contains many features which are often enabled
  through the `extensions`.
  
  [Jupyter Book][] provides a front-end to sphinx, with alternative ways of specifying the
  structure of the book.  We do not use this, but instead, following the instructions to
  manage [Jupyter Book as a Sphinx website][].  Thus, we use the [Sphinx][] build tools
  rather than running the `jupytebook` command.
* [MyST][] -- Markedly Structured Text: By default, [Sphinx][] documentation is written in
  reStructuredText ([RST][]), which is highly customizable, but not so convenient.  We use
  [MyST][] instead, which is a fairly minimal extension to [CommonMark Mardown][] that
  allows one to use [RST][] features.  Notably, the structure of the documentation is
  provided through [`toctree`][] directives.  These look like the following in [RST][]:
  
  ````rst
  .. toctree::
     :maxdepth: 2
     :caption: "Contents:"
     :titlesonly:
     :hidden:

     Overview
     Syllabus
     Assignments
     References
  ````
  
  When translating from the [Sphinx][] documentation, you will need to [rewrite
  these](https://myst-parser.readthedocs.io/en/latest/syntax/syntax.html).  For example,
  in [MyST][], this becomes

  ````markdown
  ```{toctree}
  ---
  maxdepth: 2
  caption: "Contents:"
  titlesonly:
  hidden:
  ---
  Overview
  Syllabus
  Assignments
  References
  ```
  ````
  
  *(This is from `Docs/index.md`, which specifies the start of our
  documentation.)*
* [MyST-NB][]: Our [MyST][] documents can function as [Jupyter Notebooks][] by using the
  [MyST-NB][] extension.  When the documentation is built, code cells will be executed and
  the output displayed.  This is how we make figures etc., often hiding the code so the
  presentation is clean, but having the code available for students to look at later if
  they want the details.
* [Jupytext][]: This tool allows one to convert [Jupyter Notebooks][] from one format to
  another.  In particular, if you have a notebook you would like to include in the
  documentation, you can do this with:
  
  ```bash
  jupytext --set-formats ipynb,myst Notebook.ipynb
  ```
  
  This will create an appropriate `Notebook.md` file that you can include in your
  documentation. *(You can also do this by [installing the server
  extension](https://jupytext.readthedocs.io/en/latest/install.html), which will provide
  commands to pair notebooks in your [Jupyter Notebook][] GUI under **File -> Jupytext > Pair
  Notebook with ...**.)*
  
  One caution: [CoCalc][] does [not yet support
  Jupytext](https://github.com/sagemathinc/cocalc/issues/4715) in their collaborative
  interface.  You can always do this by launching the [Classical Jupyter
  Notebook](https://doc.cocalc.com/jupyter.html#classical-versus-cocalc) interface, but
  you lose the ability to collaborate in realtime.  We are working on some hooks to make
  this a better experience.
* [Sphinx Book Theme][]: This controls the look of the generated documentation.  Look here
  for details about formatting, for example, [special content
  blocks](https://sphinx-book-theme.readthedocs.io/en/latest/content-blocks.html) for
  margin notes, etc.
:::
https://sphinx-book-theme.readthedocs.io/en/latest/customize/sidebar-primary.html

After editing your documentation, add, commit, and push your changes:

```bash
hg add Docs/...
hg commit -m "DOC: Added notes about ..."
hg push
```

If you have setup everything properly as described above, this should automatically
trigger [ReadTheDocs][] to pull and build your documentation:

* [Physics 555 Course Documentation][]

Check to make sure everything works.


### Install Documentation on CoCalc

Once you are happy with your documentation 

1.  (optional) On my personal machine, I establish an SSH connection to the [CoCalc
    shared project][] so I can connect and install everything there.  I do this by adding
    the following to my `~/.ssh/config` file on my laptop:
    
    ```
    # SSH Config file
    # dest = ~/.ssh/config  # Keep this as the 2nd line for mmf_init_setup
    
    Host ccwsu   # WSU Course project for instructors
      User c31d20a3b0af4bf7a951aa93a64395f6

    Host cc555   # Shared proejct with students
      User ebaafbe3f8cf4598a2c9e7b6c64023c4
  
    Host cc*     # Defaults for all CoCalc aliases come after
      HostName ssh.cocalc.com
      ForwardAgent yes
      SetEnv LC_HG_USERNAME=Michael McNeil Forbes <michael.forbes+python@gmail.com>
      SetEnv LC_GIT_USERNAME=Michael McNeil Forbes
      SetEnv LC_GIT_USEREMAIL=michael.forbes+python@gmail.com
      SetEnv LC_EDITOR=vi
  
    # I also have a local config file for my Mac.  I keep it in a different file
    # called config.local and include it with:
    #
    # Include config.local
    #
    # It contains the following which stores my keys in the Keychain Access app
    # so that they are authenticated when I login.

    Host *
      IgnoreUnknown UseKeychain
      UseKeychain yes
      AddKeysToAgent yes
      AddressFamily inet
      # Force IPv4
      # https://www.electricmonk.nl/log/2014/09/24/
      #         ssh-port-forwarding-bind-cannot-assign-requested-address/  
    ```
    
    Once this is done, create a private/public keypair with
    
    ```bash
    ssh-kegen -t ed25519
    ```
    
    The copy the **public key** from `.ssh/id_ed25519.pub` and store it in your
    [CoCalc/Settings/SSH Keys](https://cocalc.com/settings/ssh-keys) page.  This will
    allow you to SSH to the course project or shared project with 
    
    ```bash
    ssh ccwsu
    ```
    
    or 
    
    ```bash
    ssh cc555
    ```
    
    respectively.
    
    :::{note}
    If you get the following error, it probably means that you need to login to the
    [CoCalc][] website and start the project - you can only SSH to a running project:
    
    ```bash
    $ ssh cc555
    ebaafbe3f8cf4598a2c9e7b6c64023c4@ssh.cocalc.com: Permission denied (publickey).
    ```
    :::

    I also copy this key (or make another) to [GitLab][] and [GitHub][] so that I can
    authenticate there without a password.
    
12. Setup shared course project on CoCalc.  Once the previous SSH connections are made,
    appropriately forwarding your keys, then you should be able to clone the repos to
    the [CoCalc shared project][]:
    
    ```bash
    # Start CoCalc project by opening the shared project linked above
    ssh cc555
    mkdir .repositories
    cd .repositories
    git clone git@gitlab.com:wsu-courses/physics-555-quantum-technologies.git
    cd ~/.repositories/physics-555-quantum-technologies
    make init
    ```

## CoCalc Assignments

I use CoCalc for assignments assigned in the [CoCalc course file][].  This generally
worked well, but there are a few issues:

1. Don't use `@interact` with `nbgrader` until [issue
   6137](https://github.com/sagemathinc/cocalc/issues/6137) is fixed.
2. If you need to install things on the students accounts, then note that you can [run a
   Terminal command in all student
   projects](https://doc.cocalc.com/teaching-tips_and_tricks.html#run-terminal-command-in-all-student-projects).
   I used this to install [Qiskit][]:
   
   ```bash
   /ext/anaconda2021.11/bin/python3 -m pip install --user qiskit[visualization]
   ```
   
   I also include the following code in each notebook:
   
   ```ipython
   try:
        import qiskit
   except ImportError:
       import sys
       !{sys.executable} -m pip install --user qiskit[visualization]
       print("Restart your kernel and try again")
   ```
   

## Canvas Setup

After activating the course, I started adding some assignments.  The [first
assignment](https://wsu.instructure.com/courses/1574702/assignments/7542709) contains
instructions about how to setup [Hypothes.is][] and join the private group for
discussions.  We will need to send students an anonymized username so that their
participation cannot be tracked easily.

## Discussion Forums

I opened up a [discussion
forum](https://wsu.instructure.com/courses/1574702/discussion_topics) to give people a
place to comment on the course and ask questions.


# Hypothes.is

* https://web.hypothes.is/help/using-the-hypothesis-app-with-assignments-in-canvas/


# Quantum Networking

(From Landscape of Quantum Networking Webinar, Paul G. Kwiat, Krister Shalam)
* Photon's likely the technology of choice.
* Loss is the main issue (no repeaters).
* Commercial sources of entanglement now:
  * Generation of entanglement with parametric downs conversion: PRL 75, 4337 (1995)
  * Quantum Dots.
* Detection:
  * Avalanche photodiodes (30% efficiency for single photon detection)
  * Superconducting nanowires (>98% efficient, but need ~K T)
  * Transition edges sensors (photon-number resolving >98%)
* Multiplexing with angular momentum states: https://www.nature.com/articles/s41467-020-17616-4
* Quantum repeater?
  * Interesting application.
* Quantum key distribution requires <10% error rates.
* Building entanglement will cost quite a bit (energy).

# Issues

* [ ] `make doc-server` is not doing the right thing.  Check with Math 589... something to
  do with the index file.
  
[GitLab]: <https://gitlab.com>
[GitHub]: <https://github.com>
[ReadTheDocs]: <https://readthedocs.org/>
[CoCalc]: <https://cocalc.com> 
[GitLab course repo]: <https://gitlab.com/wsu-courses/physics-555-quantum-technologies>
[GitHub course mirror]: <https://github.com/WSU-Physics-Courses/physics-555-quantum-technologies>
[course documentation admin page]: <https://readthedocs.org/projects/physics-555-quantum-technologies>
[Physics 555 Course Documentation]: <https://physics-555-quantum-technologies.readthedocs.io/en/latest/>
[CoCalc course file]: <https://cocalc.com/projects/c31d20a3-b0af-4bf7-a951-aa93a64395f6/files/Archive/Physics555/2022-Physics-555-Fall.course>
[CoCalc shared project]: <https://cocalc.com/projects/ebaafbe3-f8cf-4598-a2c9-e7b6c64023c4/files>
[hg-git]: <https://hg-git.github.io/>
[cookiecutter course templates]: <https://hg.iscimath.org/forbes-group/cookiecutter-templates/-/tree/topic/default/initial/cookiecutter-course>
[WSU Schedule of Classes]: <https://schedules.wsu.edu/>
[Sphinx]: <https://www.sphinx-doc.org/en/master/>
[RST]: <https://docutils.sourceforge.io/rst.html>
[MyST]: <https://myst-parser.readthedocs.io/en/latest/>
[MyST-NB]: <https://myst-nb.readthedocs.io/en/latest/>
[CommonMark Markdown]: <https://commonmark.org/>
[`toctree`]: <https://www.sphinx-doc.org/en/master/usage/quickstart.html#defining-document-structure>
[Jupyter Book]: <https://jupyterbook.org/intro.html>
[Jupyter Book as a Sphinx website]: <https://jupyterbook.org/sphinx/index.html#convert-your-jupyter-book-into-a-sphinx-website>
[Jupyter Notebook]: <https://jupyter.org/>
[Jupytext]: <https://jupytext.readthedocs.io/en/latest/>
[Sphinx Book Theme]: <https://sphinx-book-theme.readthedocs.io/en/latest/>
